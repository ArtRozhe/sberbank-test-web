import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { NotFoundComponent } from './not-found/not-found.component';
import { WeatherComponent } from './weather/weather.component';
import { WeatherCurrentComponent } from './weather/components/weather-current/weather-current.component';
import { WeatherChooseLocationComponent } from './weather/components/weather-choose-location/weather-choose-location.component';

const routes: Routes = [
  {
    path: '',
    component: WeatherComponent,
    children: [
      {
        path: '',
        component: WeatherChooseLocationComponent
      },
      {
        path: 'weather',
        component: WeatherCurrentComponent
      }
    ]
  },
  {
    path: '**',
    component: NotFoundComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)]
})
export class AppRoutingModule { }
