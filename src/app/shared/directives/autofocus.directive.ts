import { Directive, Input, ElementRef, OnInit } from '@angular/core';

/**
 * Autofocus on the selected element
 */
@Directive({
  selector: '[appAutofocus]'
})
export class AutofocusDirective implements OnInit {
  /**
   * Element with autofocus
   */
  private element: any;

  constructor(private elementRef: ElementRef) {
    this.element = this.elementRef.nativeElement;
  }

  ngOnInit() {
    this.element.focus();
  }
}
