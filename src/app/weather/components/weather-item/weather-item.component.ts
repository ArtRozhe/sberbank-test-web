import { Component, OnInit } from '@angular/core';
import { Input } from '@angular/core';
import { IWeatherItem } from './weather-item';

/**
 * Displaying weather item
 */
@Component({
  selector: 'app-weather-item',
  templateUrl: './weather-item.component.html',
  styleUrls: ['./weather-item.component.css']
})
export class WeatherItemComponent implements OnInit {
  @Input() item: IWeatherItem;

  /**
   * @ignore
   */
  constructor() { }
  
  /**
   * @ignore
   */
  ngOnInit() { }
}
