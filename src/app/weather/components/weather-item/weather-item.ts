import { ILocation } from '../../../core/services/location/location';

interface IWeatherWind {
  speed: number;
  deg: number;
  direction: string;
}

interface IWeatherMain {
  temp: number;
  humidity: number;
  pressure: number;
  tempMin: number;
  tempMax: number;
}

interface IWeatherDesc {
  name: string;
  code: number;
}

export interface IWeatherItem {
  coordinate: ILocation,
  main: IWeatherMain,
  wind: IWeatherWind;
  description: IWeatherDesc;
}
