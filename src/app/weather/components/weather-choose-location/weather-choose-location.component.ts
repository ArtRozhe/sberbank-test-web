import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

import { LocationAbstract } from '../../../core/services/location/location.abstract';

/**
 * Selecting the location
 */
@Component({
  selector: 'app-weather-choose-location',
  templateUrl: './weather-choose-location.component.html',
  styleUrls: ['./weather-choose-location.component.css']
})
export class WeatherChooseLocationComponent implements OnInit {
  constructor(private router: Router, private locationService: LocationAbstract, private formBuilder: FormBuilder) { }
  error: string;
  chooseForm: FormGroup;
  locations = null;
  locationsLoading = false;

  ngOnInit() {
    this.locationsLoading = true;
    this.locationService.getLocations()
      .subscribe(locations => {
        this.locations = this.getFormattedLocations(locations);
        this.locationsLoading = false;
      },
        () => {
        this.locationsLoading = false;
        this.error = 'Произошла ошибка получения данных. Пожалуйста, попробуйте позже.';
      });

    this.initForm();
  }

  getFormattedLocations(data) {
    return data.map(location => {
      return {
        value: location.city,
        label: location.city
      };
    });
  }

  isControlInvalid(controlName: string): boolean {
    const control = this.chooseForm.controls[controlName];

    return control.invalid && control.touched;
  }

  initForm() {
    this.chooseForm = this.formBuilder.group({
      location: [null, [
        Validators.required
      ]]
    });
  }

  /**
   * Choose the location
   * @param {string} location
   */
  choose(location: string) {
    this.router.navigate(['weather'], {queryParams: {location: location}});
  }

  /**
   * Submitting choosing form
   */
  onSubmit() {
    const controls = this.chooseForm.controls;

    if (this.chooseForm.invalid) {
      Object.keys(controls)
        .forEach(controlName => controls[controlName].markAsTouched());

      return;
    }

    this.choose(controls['location'].value);
  }
}
