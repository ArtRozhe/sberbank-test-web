import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { IWeatherItem } from '../weather-item/weather-item';
import { ILocation } from '../../../core/services/location/location';
import { LocationAbstract } from '../../../core/services/location/location.abstract';
import { CurrentWeatherAbstract } from '../../services/current-weather/current-weather.abstract';

/**
 * Displaying the current weather for the selected location
 */
@Component({
  selector: 'app-weather-current',
  templateUrl: './weather-current.component.html',
  styleUrls: ['./weather-current.component.css']
})
export class WeatherCurrentComponent implements OnInit {
  city: string;
  location: ILocation;
  error: string;
  position: number;
  current: IWeatherItem;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private locationService: LocationAbstract,
    private currentWeatherService: CurrentWeatherAbstract
  ) { }

  /**
   * Handling position of the user
   * @param {number} position
   */
  private onPositionReceived(position: number) {
    this.position = position;
  }

  /**
   * Converts wind degrees to the string directions
   * @param {number} degrees
   * @returns {string}
   */
  private getDirectionWindByDegrees(degrees: number) {
    const directions = [
      'С', 'СВ', 'В', 'ЮВ', 'Ю', 'ЮЗ', 'З', 'СЗ'
    ];

    const val =  Math.floor((degrees / 45) + 0.5);
    return directions[(val % 8)];
  }

  /**
   * Getting formatted weather data
   * @param {IWeatherItem} data
   */
  private getFormattedWeather(data): IWeatherItem {
    data.wind.direction = this.getDirectionWindByDegrees(data.wind.deg);
    return data;
  }

  /**
   * Handling current weather
   * @param {object} data
   */
  private onWeatherDataReceived(data: any) {
    this.current = this.getFormattedWeather(data);
  }

  /**
   * Getting weather by the location
   * @param {ILocation} location
   */
  private getWeather(location: ILocation) {
    this.location = location;

    const weather = this.currentWeatherService.getWeather(this.location, (errorMessage) => {
      this.error = errorMessage;
    });

    weather
      .subscribe(
        weatherData => {
          if (weatherData.position || weatherData.position === 0) {
            this.onPositionReceived(weatherData.position);
          } else if (weatherData.coordinate) {
            this.onWeatherDataReceived(weatherData);
          }
        },
        error => {
          this.error = error.message;
        });
  }

  /**
   * Getting location by the city
   * @param {string} city
   */
  private getLocation(city) {
    this.locationService.getLocation(city)
      .subscribe(
        data => {
          if (!data) {
            this.error = 'Город не найден. Попробуйте выбрать другой город.';
            return;
          }

          if (data.longitude && data.latitude) {
            this.getWeather(data);
          }
        },
        error => {
          this.error = error.message;
        });
  }

  /**
   * Updating weather data of the current location
   */
  private update() {
    this.error = '';
    this.current = null;
    this.position = null;

    this.getLocation(this.city);
  }

  ngOnInit() {
    this.route.queryParams.subscribe(params => {
      const city = params['location'];

      if (city === '' || !city) {
        this.router.navigate(['']);
        return;
      }

      this.city = city;

      this.getLocation(this.city);
    });
  }
}
