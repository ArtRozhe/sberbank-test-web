import { Observable } from 'rxjs';
import { ILocation } from '../../../core/services/location/location';

/**
 * Represents a service that provides getting the current weather
 */
export abstract class CurrentWeatherAbstract {
  /**
   * Getting the current weather
   * @param {ILocation} coordinates
   * @param {Function} errorCallback
   * @returns {Observable<any>}
   */
  abstract getWeather(location: ILocation, errorCallback: (errorMessage: any) => void): Observable<any>;
}
